var http = require("http");
var fs = require("fs");
var serial = require("serialport");
var SerialPort = require("serialport").SerialPort;
var hue = require("./huesync.js");
var sleep = require("sleep");

console.log(serial.SerialPort);
const { exec } = require('child_process');
qs = require('querystring');

var lastaction = "";

serial.list(function(err, ports) {
	ports.forEach(function(port) {
		console.log(port.comName);
	});
});

var device = process.argv[2];
console.log("Using device: " + device);

var port = new serial(device, {
	baudRate: 9600//,
  //parser: serial.parsers.Readline
});

var parser = new serial.parsers.Readline("\r\n");

port.pipe(parser);


port.write("3");

serialdata = "";
parser.on('data', (data) => {
  console.log(data);
  serialdata = data;
});

var server = http.createServer(function(request, response) {

  var constreg = /\/lightstate\/constant\/([0-9a-f]{6})$/i;
  var dfadereg = /\/lightstate\/dualfade\/([0-9a-f]{6})([0-9a-f]{6})$/i;

  var endResp = true;

  try {
    if(request.headers.origin != undefined) {
      response.setHeader('Access-Control-Allow-Headers', request.headers.origin);
      response.setHeader('Access-Control-Allow-Origin', request.headers.origin);
    }
  } catch(ex) {
    console.log("COULD NOT SET CORS HEADERS!");
  }


  if(request.method == "GET") {
    if(request.url == "/lightstate/on") {
      console.log("ON");
      port.write("3");
      response.write("ON");
    } else if(request.url == "/lightstate/off") {
      console.log("OFF");
      port.write("2");
      response.write("OFF");
    } else if(request.url == "/lightstate/rgbon") {
      console.log("RGBON");
      port.write("1");
      response.write("RGBON");
    } else if(request.url == "/lightstate/rgboff") {
      console.log("RGBOFF");
      port.write("0");
      response.write("RGBOFF");
    } else if(request.url == "/lightstate/spectrum") {
      console.log("SPECTRUM");
      port.write("1");
      response.write("SPECTRUM");
    } else if(request.url == "/lightstate/sync") {
      console.log("SYNC");
      hue.getHexFromHue("2", 0, (hex) => { port.write("4" + hex); });
      response.write("SYNC");
    } else if(request.url == "/lightstate/syncinv") {
      console.log("SYNCINV");
      hue.getHexFromHue("2", 180.0, (hex) => { port.write("4" + hex); });
      response.write("SYNCINV");
    } else if(constreg.test(request.url)) {
      console.log("CONSTANT");
      var match = constreg.exec(request.url);
      port.write("4" + match[1]);
      response.write("CONSTANT");
    } else if(dfadereg.test(request.url)) {
      console.log("DUALFADE");
      var match = dfadereg.exec(request.url);
      port.write("5" + match[1] + match[2]);
      response.write("DUALFADE");
    } else if(request.url == "/lightstate/dualfadesync") {
      console.log("DUALFADESYNC");

      hue.getHexFromHue("1", 0, (hex) => { 
        portstr = hex;
        hue.getHexFromHue("3", 0, (hex2) => {
          portstr = portstr + hex2;
          port.write("5" + portstr);
          console.log(portstr);
        });
      });

      response.write("DUALFADESYNC");
    } else if(request.url == "/lightstate/dualfadesyncinv") {
      console.log("DUALFADESYNCINV");

      hue.getHexFromHue("1", 180.0, (hex) => { 
        portstr = hex;
        hue.getHexFromHue("3", 180.0, (hex2) => {
          portstr = portstr + hex2;
          port.write("5" + portstr);
        });
      });

      response.write("DUALFADESYNCINV");
    } else if(request.url == "/lightstate") {
      endResp = false;
      port.write("?")
      setTimeout(() => {
        if(response.writableEnded)
          console.log(">:[");

        var substridx = serialdata.length;
        for(var i = 0; i < serialdata.length - 1; i++) {
          var seq1 = serialdata.charCodeAt(i);
          var seq2 = serialdata.charCodeAt(i + 1);

          if(seq1 == 0x1 && seq2 == 0xFFFD) {
            substridx = i;
            break;
          }
        }

        response.write(serialdata.slice(0, substridx).trim() + ";");
        response.end();
      }, 500);
    }

    if(endResp)
      response.end();
  }
});

server.listen(8181);
console.log("started on 8181");
